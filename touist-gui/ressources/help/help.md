# Touist help
### What is Touist for

### Quick start


### Advanced use
You can choose between SAT and SMT:
- SAT accepts only simple prepositions that you would use in a formula. For example `a and b or c`
- SMT can handle arithmetic expressions instead of simple prepositions: `a and b or (x>4)`

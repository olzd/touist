package translation;

/**
 * This class holds one error that might be generated by the translator,
 * with the format (row,column,error_text)
 * @author maelv
 */
public class TranslationError {
	private int rowInCode;
	private int columnInCode;
	private boolean rowAndColumnKnown;
	private String errorMessage;

	public TranslationError(int rowInCode, int columnInCode, String errorMessage) {
		this.rowInCode = rowInCode;
		this.columnInCode = columnInCode;
		this.errorMessage = errorMessage;
		rowAndColumnKnown = true;
	}
	
	public TranslationError(String errorMessage) {
		this.errorMessage = errorMessage;
		rowAndColumnKnown = false;
	}

	@Override
	public String toString() {
		return (hasRowAndColumn())
				?"Line "+rowInCode+", column "+columnInCode+": "+errorMessage
				:errorMessage;
	}

	public boolean hasRowAndColumn() {
		return rowAndColumnKnown;
	}
	
	public int getRowInCode() {
		return rowInCode;
	}

	public int getColumnInCode() {
		return columnInCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
